FROM docker.io/kernelci/qemu
RUN mkdir -p /etc/qemu
RUN echo allow all > /etc/qemu/bridge.conf

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        u-boot-qemu
